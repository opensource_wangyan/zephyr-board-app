#!/bin/sh
# 将mcuboot和app结合生成一个bin文件
# 格式 [cmd mcuboot.bin size app.bin]
APP_PATH=$(dirname $(readlink -f "$0"))/../../../
die() {
	echo -e "\033[31m ERROR: $@ \033[0m"
	exit 1
}

log() {
    echo -e "\033[32m LOG: $@ \033[0m"
}
APP_NAME=$3
APP_BOOT_NAME=${APP_NAME%.bin}_mcuboot.bin
log "APP_BOOT_NAME=$APP_BOOT_NAME"

if [ ! $# -eq 3 ];then
    die "input ./createapp.sh mcubootname.bin size/K appname.bin"
fi
    # 进入应用空间
    cd $APP_PATH/out
# 查看app应用是否存在
if [ ! -f $3 ];then
    die "$3.bin is not find!"
fi
# 查看mcuboot是否存在
if [ ! -f $1 ];then
    die "$1.bin is not find!"
fi
# 判定输入是数字
if [ ! -n "$(echo $2| sed -n "/^[0-9]\+$/p")" ];then
    die "$2 is not number!"
fi
    log "size=$2"
    APP_NAME=$3
    APP_BOOT_NAME=${APP_NAME%.bin}_mcuboot.bin
    log "APP_BOOT_NAME=$APP_BOOT_NAME"
if [ -f $APP_BOOT_NAME ];then
    @rm -rf $APP_BOOT_NAME
fi

    # 创建目标文件
    # dd if=/dev/zero of=./tmp.bin bs=${2}K count=1
    dd if=$1 of=$APP_BOOT_NAME bs=${2}K conv=sync
    cat $3 >> $APP_BOOT_NAME

    # dd if=$1 of=./tmp.bin bs=${2}K count=1 conv=notrunc
    # dd if=./tmp.bin  of=$APP_BOOT_NAME seek=${2}K count=1 || die "creste zero file fail"
     




