# SPDX-License-Identifier: Apache-2.0

board_runner_args(openocd)

if(CONFIG_BOARD_EFR32MG21)
board_runner_args(jlink "--device=EFR32MG21AxxxF1024")
endif()

include(${ZEPHYR_BASE}/boards/common/openocd.board.cmake)
include(${ZEPHYR_BASE}/boards/common/jlink.board.cmake)
