#!/bin/sh

die() {
	echo "\033[31m ERROR:$@\033[0m"
	exit 1
}

log() {
    echo "\033[32m LOG:$@\033[0m"
}

if [ ! -d out ]; then
    die "out dir is not\r\n"
fi
if [ ! -f ./out/.tmp ]; then
    die "BOARD_TAG is val null\r\n"
fi

BOARD_TAG=`cat ./out/.tmp`

SEARCHE_DIR="boards/arm/${BOARD_TAG}/support/"
echo ${SEARCHE_DIR}
if [ ! -d out ]; then
    die "out dir is not\r\n"
fi

openocd.exe -s ${SEARCHE_DIR} -f target.cfg  -c "init" -c "reset init" -c "nrf5 mass_erase 0" -c "program ./out/${BOARD_TAG}_taget.bin verify reset exit"
