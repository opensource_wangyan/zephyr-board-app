#!/bin/sh
export ZEPHYR_SDK_INSTALL_DIR=~/zephyr-sdk-0.13.0-alpha

APP_PATH=`pwd`
CLEAN_SWITCH=0
MCUBOOT_PATH=$APP_PATH/../bootloader/mcuboot/boot/zephyr
BUILD_PATH=build
MCU_OFFICE=64               #mcu_boot的偏移/k单位
KEYS_FILE=root-rsa-2048.pem # 只需要文件名字，路径会自己添加
BOARD_TAG="qingfeng_nrf52832"
die() {
	echo "\033[31m ERROR:$@\033[0m"
	exit 1
}

log() {
    echo "\033[32m LOG:$@\033[0m"
}

# wangyan 获取输入
while getopts 'b:n:ck:o:' OPT; do
    case $OPT in
        b)
            BOARD_TAG="$OPTARG";;
        c)
            CLEAN_SWITCH=1;;
        k)
            KEYS_FILE="$OPTARG";;
        o)
            MCU_OFFICE="$OPTARG";;
        ?)
            log "Usage: `basename $0` [-a filename|-a address|-f|-c|-k keyfile|-o mcubootoffice]"
    esac
done
APP_BIN="${BOARD_TAG}.bin"    # 默认的app的二进制文件名字
MCUBOOT_BIN="mcuboot_${BOARD_TAG}.bin"   # 默认的mcuboot生成的二进制文件名

log "APP_BIN=$APP_BIN"
log "CLEAN_SWITCH=$CLEAN_SWITCH"
log "KEYS_FILE=$KEYS_FILE"
log "MCU_OFFICE=$MCU_OFFICE"
log "BOARD_TAG=$BOARD_TAG"
log "APP_BIN=$APP_BIN"
log "MCUBOOT_BIN=$MCUBOOT_BIN"

shift $(($OPTIND - 1))

if [ $CLEAN_SWITCH -eq 1 ];then
    # 清理mcu的build目录
    if [ -d $MCUBOOT_PATH/$BUILD_PATH ]; then
        rm -rf $MCUBOOT_PATH/$BUILD_PATH
    fi
    # 清理app的build目录
    if [ -d $APP_PATH/$BUILD_PATH ]; then
        rm -rf $APP_PATH/$BUILD_PATH
    fi
    # 清理out目录
    if [ -d $APP_PATH/out ]; then
        rm -rf $APP_PATH/out
    fi
    log "cleaning ok.........."
fi
if [ ! -d $APP_PATH/out ]; then
        mkdir $APP_PATH/out
fi
# mcuboot处理
if [ ! -f $APP_PATH/out/$MCUBOOT_BIN ]; then
    # 改变到mcuboot目录下 ，编译mcuboot
    # log "path=$MCUBOOT_PATH"
    cd $MCUBOOT_PATH

    # 编译 mcuboot
    log "APP_PATH=$APP_PATH"
    west build -p -b ${BOARD_TAG} -- -DCONF_FILE=${APP_PATH}/mcuboot.conf || die "build error!!"
    #west build -p -b ${BOARD_TAG}
    if [ ! -f $BUILD_PATH/zephyr/zephyr.bin ]; then
        die "Don't find zephyr.bin"
    fi

    if [ -d $APP_PATH/out ]; then
        mv $BUILD_PATH/zephyr/zephyr.bin $APP_PATH/out/$MCUBOOT_BIN || die "Don't find mcu zephyr.bin"
        echo "The mcu bin file is move in the ./out dirctory----"
    fi
fi

    # 改变到应用目录下,开始编译应用
    cd $APP_PATH
# 删除上次生成的文件
if [ -f out/$APP_BIN ]; then
    rm -rf out/$APP_BIN
fi
if [ -f out/${APP_BIN%.bin}_taget.bin ]; then
    rm -rf out/${APP_BIN%.bin}_taget.bin
fi
    # 编译app
	west build -p -b ${BOARD_TAG} || die "build error!!"
    # 对app加密，todo
    west sign -t imgtool -- --key keys/$KEYS_FILE || die "sign error!!"

# 移动生成的文件到指定目录下
if [ ! -f $BUILD_PATH/zephyr/zephyr.signed.bin ]; then
    die "Don't find zephyr.signed.bin"
fi

if [ -d out ]; then
	mv $BUILD_PATH/zephyr/zephyr.signed.bin out/$APP_BIN || die "Don't find zephyr.bin"
	echo "The bin file is move in the ./out dirctory----"
fi

    # 将mcu和app整合一个bin
    #sh $APP_PATH/scripts/west_commands/shell/creatapp.sh $MCU_BIN $MCU_OFFICE $APP_BIN || die "creatapp is fail"
    # 进入out目录
    cd $APP_PATH/out

    dd if=$MCUBOOT_BIN of=${APP_BIN%.bin}_taget.bin bs=${MCU_OFFICE}K conv=sync
    cat $APP_BIN >> ${APP_BIN%.bin}_taget.bin

    # 返回目录
    cd $APP_PATH
if [ ! -f out/${APP_BIN%.bin}_taget.bin ]; then
    die "creat bin file error"
fi
# 将目标导出去，为后面的安装使用
echo ${BOARD_TAG} > out/.tmp

echo "Done."