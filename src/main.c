/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <sys/printk.h>
#include <devicetree.h>
#include <drivers/gpio.h>
#include <logging/log.h>
#include <shell/shell.h>
#include <stdlib.h>
#include "driver_led.h"

#ifdef CONFIG_SHELL_START_OBSCURED
#include "shell_login.h"
#endif

LOG_MODULE_REGISTER(main,CONFIG_MAIN_MODULE_LOG_LEVEL);

void main(void)
{
	led_s *led_dev;
#ifdef CONFIG_SHELL_START_OBSCURED
	if (IS_ENABLED(CONFIG_SHELL_START_OBSCURED)) {
		login_init();
	}
#endif
	//初始化led
	led_dev=led_init();
	if(led_dev==NULL){
		LOG_ERR("led init error!");
	}
	while (1) {
		led_switch(led_dev);
		LOG_INF("led status is %d!",led_dev->led_status);
		k_msleep(SLEEP_TIME_MS);
	}
	//销毁led
	led_destory(led_dev);
}
