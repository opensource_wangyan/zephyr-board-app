#include "driver_led.h"

led_s* led_init(void) {
    led_s *led_dev;
    int ret = 0;
    led_dev = k_malloc(sizeof(led_s));
    led_dev->dev = device_get_binding(LED0);
    led_dev->led_status = false; //初始为灭

	if (led_dev->dev == NULL) {
		return NULL;
	}

	ret = gpio_pin_configure(led_dev->dev, PIN, GPIO_OUTPUT_ACTIVE | FLAGS);
	if (ret < 0) {
		return NULL;
	}
    return led_dev;
}

void led_destory(led_s *dev) {
    if(dev!=NULL){
        k_free(dev);
        dev=NULL;
    }
}

int led_on(led_s *dev) {
    if(dev==NULL||dev->dev==NULL){
        return -1;
    }
    gpio_pin_set(dev->dev, PIN, 1);
    dev->led_status = true;
    return 0;
}

int led_off(led_s *dev) {
    if(dev==NULL||dev->dev==NULL){
        return -1;
    }
    gpio_pin_set(dev->dev, PIN, 0);
    dev->led_status = false;
    return 0;
}

int led_switch(led_s *dev) {
    if(dev==NULL||dev->dev==NULL){
        return -1;
    }
    if(dev->led_status){
        return led_off(dev);
    }else{
        return led_on(dev);
    }
}