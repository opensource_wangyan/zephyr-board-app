#ifndef _APP_DRIVER_MODULE_LED_H_
#define _APP_DRIVER_MODULE_LED_H_
#include <zephyr.h>
#include <device.h>
#include <devicetree.h>
#include <drivers/gpio.h>
#include <logging/log.h>

/* 1000 msec = 1 sec */
#define SLEEP_TIME_MS   1000

/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led1)

#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
#define LED0	DT_GPIO_LABEL(LED0_NODE, gpios)
#define PIN	DT_GPIO_PIN(LED0_NODE, gpios)
#define FLAGS	DT_GPIO_FLAGS(LED0_NODE, gpios)
#else
/* A build error here means your board isn't set up to blink an LED. */
#error "Unsupported board: led0 devicetree alias is not defined"
#define LED0	""
#define PIN	0
#define FLAGS	0
#endif

typedef struct led_struct{
    const struct device *dev;
	bool led_status; //true:亮 false:灭
} led_s;

led_s* led_init(void);
void led_destory(led_s *dev);
int led_on(led_s *dev);
int led_off(led_s *dev);
int led_switch(led_s *dev);

#endif