#ifndef _APP_SHELL_MODULE_LOGIN_H_
#define _APP_SHELL_MODULE_LOGIN_H_

#include <logging/log.h>
#include <shell/shell.h>
#include <version.h>
#include <stdlib.h>

void login_init(void);
int cmd_login(const struct shell *shell, size_t argc, char **argv);
int cmd_logout(const struct shell *shell, size_t argc, char **argv);

#endif